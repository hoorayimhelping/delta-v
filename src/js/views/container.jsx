import React from 'react';
import ReactDOM from 'react-dom';

import SolarSystem from '../maps/solar_system';
import KerbolSystem from '../maps/kerbol_system';

module.exports = React.createClass({
  render: function() {
    return (
      <div>
        <h1>Hello, world!</h1>
        <h2>Yeah!</h2>
      </div>
    );
  }
});
